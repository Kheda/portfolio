import classNames from "classnames";
import React from "react";
import styles from "./Header.module.scss";

const cx = classNames.bind(styles);


const Header: React.FC = () => {
    return (
        <div className={cx(styles.Header)}>
            <div className={cx(styles.logoBlock)}>
                <a href="" className={cx(styles.Logo)}>
                    <h1>K<span>h</span></h1>
                    <h3>Kheda</h3>
                </a>
                <p>Web-developer</p>
            </div>
            <nav className={cx(styles.mainNav)}>
                <ul className={cx(styles.mainNavItems)}>
                    <li><a href="">About</a></li>
                    <li><a href="">My Skills</a></li>
                    <li><a href="">Work</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contact</a></li>
                </ul>
            </nav>
            <ul className={cx(styles.mainNavSocial)}>
                {/* <li><a href="">telegram</a></li>
                <li><a href="">instagram</a></li>
                <li><a href="">github</a></li> */}
            </ul>
        </div>
    );
};

export default Header;