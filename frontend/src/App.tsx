import React from 'react';
import './App.css';
import Header from './components/Headers/Header';

function App() {
  return (
    <div className="App">
      <Header/>
    </div>
  );
}

export default App;
