from django.contrib import admin
from .models import Works, Posts, ProjectScreens

admin.site.register(Works)
admin.site.register(Posts)
admin.site.register(ProjectScreens)
