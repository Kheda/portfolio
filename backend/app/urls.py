from django.urls import path
from . import views

urlpatterns = [
    path('works/', views.WorksView.as_view()),
    path('works/<int:id>/', views.WorkView.as_view()),
    path('posts/', views.PostsView.as_view()),
    path('posts/<int:id>/', views.PostView.as_view()),
]
