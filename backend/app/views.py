from app.models import Works
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from app.types import WorksSerializer
from app.models import Posts
from app.types import PostsSerializer

class WorkView(APIView):
    def get(self, request, **kwargs):
        workId = kwargs['id']
        try:
            work = Works.objects.get(id=workId)
            serializer = WorksSerializer(work)
            return Response({"work": serializer.data})
        except Works.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

class WorksView(APIView):
    def get(self, request):
        category = request.GET.get("category")
        works = Works.objects.all()
        if category:
            works = Works.objects.filter(category=category)
        serializer = WorksSerializer(works, many=True)
        return Response({"works": serializer.data})



class PostView(APIView):
    def get(self, request, **kwargs):
        postId = kwargs['id']
        try:
            post = Posts.objects.get(id=postId)
            serializer = PostsSerializer(post)
            return Response({"post": serializer.data})
        except Posts.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

class PostsView(APIView):
    def get(self, request):
        category = request.GET.get("category")
        posts = Posts.objects.all()
        if category:
            posts = Posts.objects.filter(category=category)
        serializer = PostsSerializer(posts, many=True)
        return Response({"posts": serializer.data})