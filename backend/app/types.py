from imp import source_from_cache
from rest_framework import serializers
from app.models import Works
from app.models import Posts
from app.models import ProjectScreens


class ImageSerializer(serializers.ModelSerializer): 
    class Meta:
        model = ProjectScreens
        fields = "__all__"


class WorksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Works
        fields = "__all__"
    screens_obj = ImageSerializer(source = "screens", many=True)



class PostsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Posts
        fields = "__all__"