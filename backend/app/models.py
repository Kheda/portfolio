from django.db import models
from ckeditor.fields import RichTextField

class ProjectScreens(models.Model):
    title = models.CharField('Title', max_length=150)
    image = models.ImageField(upload_to='media/', null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "ProjectScreen"
        verbose_name_plural = "ProjectScreens"


class Works(models.Model):
    title = models.CharField('Title', max_length=150)
    screens = models.ManyToManyField(ProjectScreens)
    link = models.CharField('Link', max_length=1500, null=True, blank=True)
    MY_CHOICES = (
        ("1", "HTML & CSS"),
        ("2", "JavaScript"),
        ("3", "React"),
        ("4", "Django"),
        ("5", "Node.js"),
    )
    category = models.CharField('Category', max_length=100, choices=MY_CHOICES, default='1')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Work"
        verbose_name_plural = 'Works'


class Posts(models.Model):
    title = models.CharField('Title', max_length=150)
    image = models.ImageField(upload_to='media/', null=True, blank=True)
    post = RichTextField('Post')
    MY_CHOICES = (
        ("1", "HTML & CSS"),
        ("2", "JavaScript"),
        ("3", "SQL & Databases"),
        ("4", "Web Development"),
        ("5", "React"),
    )
    category = models.CharField('Category', max_length=100, choices=MY_CHOICES, default='1')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = 'Posts'